package net.zb.examination.find.tools;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/19, 初始化版本
 * @version 1.0
 **/
public class VistorHelper {



	public static void findTheSame() throws IOException {
		Files.walkFileTree(Paths.get("files//1//"), new FileVisitor<Path>() {
			@Override
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				File tarFile = new File("files//2//" + file.getFileName().toString());
				if(tarFile.exists()){
					writeTheSame(new File(file.toUri()), tarFile);
				}
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				return FileVisitResult.CONTINUE;
			}
		});
	}



	private static void writeTheSame(File file1, File file2){
		try {
			Set<String> s1 = readFile(file1);
			Set<String> s2 = readFile(file2);
			s1.retainAll(s2);
			File file = new File("files//same" +".txt");
			if(!file.exists()){
				file.createNewFile();
			}
			if(CollUtil.isNotEmpty(s1)){
				StringBuilder sb = new StringBuilder();
				for (String s : s1){
					sb.append(s).append("\r\n");
				}
				Files.write(Paths.get(file.getPath()), sb.toString().getBytes(), StandardOpenOption.APPEND);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}



	private static Set<String> readFile(File path) throws IOException {
		Set<String> result = new HashSet<>();
		FileInputStream fin = new FileInputStream(path);
		FileChannel fcin = fin.getChannel();
		ByteBuffer buffer = ByteBuffer.allocate(256 * 1024 * 1024);
		for (; ; ) {
			buffer.clear();
			int flag = fcin.read(buffer);
			if (flag == -1) {
				break;
			}
			buffer.flip();
			Charset charset = Charset.forName("UTF-8");
			String receiveText = charset.newDecoder().decode(buffer.asReadOnlyBuffer()).toString();
			if(StrUtil.isNotEmpty(receiveText)){
				String[] array = receiveText.split("\r\n");
				Collections.addAll(result, array);
			}
		}
		return result;
	}


	public static void main(String[] args) throws IOException {
		findTheSame();
	}
}
