package net.zb.examination.find.tools;

import cn.hutool.core.util.StrUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/19, 初始化版本
 * @version 1.0
 **/
public class SplitHelper {




	public static void splitFile(String fileName, String tarDir) throws IOException {
		FileInputStream fin = new FileInputStream(fileName);
		FileChannel fcin = fin.getChannel();
		ByteBuffer buffer = ByteBuffer.allocate(256*1024*1024);
		for (;;){
			buffer.clear();
			int flag = fcin.read(buffer);
			if(flag == -1){
				break;
			}
			buffer.flip();
			Charset charset = Charset.forName("UTF-8");
			String receiveText =charset.newDecoder().decode(buffer.asReadOnlyBuffer()).toString();
			splitAndWrite(receiveText, tarDir);
		}
	}



	private static void splitAndWrite(String text, String tarDir){
		if(StrUtil.isEmpty(text)){
			return;
		}
		String[] textArray = text.split("\r\n");
		int h;
		for (String t : textArray){
			int fileName = (h = t.hashCode()) ^ (h >>> 16);
			File file = new File(tarDir + fileName +".txt");
			try {
				if(!file.exists()){
					file.createNewFile();
				}
				Files.write(Paths.get(file.getPath()), (t + "\r\n").getBytes(), StandardOpenOption.APPEND);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
