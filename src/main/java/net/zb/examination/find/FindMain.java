package net.zb.examination.find;

import net.zb.examination.find.tools.SplitHelper;
import net.zb.examination.find.tools.VistorHelper;

import java.io.IOException;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/19, 初始化版本
 * @version 1.0
 **/
public class FindMain {



	public static void main(String[] args) throws IOException {
		SplitHelper.splitFile("files/1.txt", "files//1//");
		SplitHelper.splitFile("files/2.txt", "files//2//");
		VistorHelper.findTheSame();
	}

}
